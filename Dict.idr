module Dict

import PipeOperators

%access public export


Dict : Type -> Type -> Type
Dict k v = List (k, v)


get : Eq k => k -> Dict k v -> Maybe v
get key dict =
    dict |> filter (fst||> (== key)) ||> head' |> map snd


remove : Eq k => k -> Dict k v -> Dict k v
remove key dict =
    dict |> filter (fst ||> (/= key))


upsert : Eq k => k -> v -> Dict k v -> Dict k v
upsert key value dict =
    (key, value) :: remove key dict
