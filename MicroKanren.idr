module MicroKanren

import PipeOperators
import Dict


%access public export


Var : Type
Var = Nat


data Term a
    = LVar Var
    | LVal a
    | Pair (Term a) (Term a)


Show a => Show (Term a) where
    show (LVar var) = "(LVar " ++ show var ++ ")"
    show (LVal val) = "(LVal " ++ show val ++ ")"
    show (Pair u v) = "(Pair " ++ show u ++" " ++ show v ++ ")"


Substitution : Type -> Type
Substitution a =
    Dict Var (Term a)


walk : Term a -> Substitution a -> Term a
walk term sub =
    case term of
        LVar v =>
            case get v sub of
                Just u =>
                    walk u sub
                Nothing =>
                    term
        _ => term

extend : Var -> Term a -> Substitution a -> Substitution a
extend var term sub =
     upsert var term sub


unify : Eq a => Term a -> Term a -> Substitution a -> Maybe (Substitution a)
unify term1 term2 sub =
    let
        u1 = walk term1 sub
        v1 = walk term2 sub
    in case (u1, v1) of
        (LVar n, LVar m) =>
            if n == m then Just sub else Nothing
        (LVal x, LVal y) =>
            if x == y then Just sub else Nothing
        (LVar n, _) =>
            Just (extend n v1 sub)
        (_, LVar m) =>
            Just (extend m u1 sub)
        (Pair x y, Pair x1 y1) =>
            case unify x x1 sub of
                Just s1 => unify y y1 s1
                Nothing => Nothing
        _ =>
            Nothing


State : Type -> Type
State a =
    (Substitution a, Var)


data Stream a
    = Empty
    | Immature (() -> MicroKanren.Stream a)
    | Mature (State a) (MicroKanren.Stream a)


Goal : Type -> Type
Goal a =
    State a -> MicroKanren.Stream a


unit : Goal a
unit =
    \state => Mature state Empty

mzero : MicroKanren.Stream a
mzero = Empty

mplus : MicroKanren.Stream a -> MicroKanren.Stream a -> MicroKanren.Stream a
mplus s1 s2 =
    case s1 of
        Empty =>
            s2
        Immature f =>
            Immature (\() => mplus s2 (f ()))
        Mature sc stream =>
            Mature sc (mplus s2 stream)


Semigroup (MicroKanren.Stream a) where
    (<+>) = mplus


Semigroup (MicroKanren.Stream a) => Monoid (MicroKanren.Stream a) where
    neutral = Empty


bind : MicroKanren.Stream a -> Goal a -> MicroKanren.Stream a
bind s g =
    case s of
        Empty =>
            mzero
        Immature stream =>
            Immature (\() => bind (stream ()) g)
        Mature sc stream =>
            mplus (g sc) (bind stream g)


identical : Eq a => Term a -> Term a -> Goal a
identical u v =
    \(sub, count) =>
        case unify u v sub of
            Just s1 =>
                unit ( s1, count)
            Nothing =>
                mzero


callFresh : (Term a -> Goal a) -> Goal a
callFresh f =
    \(sub, count) =>
        f (LVar count) (sub, count+1)


disjoin : Goal a -> Goal a -> Goal a
disjoin g1 g2 =
    \sc =>
        mplus (g1 sc) (g2 sc)


conjoin : Goal a -> Goal a -> Goal a
conjoin g1 g2 =
    \sc =>
        bind (g1 sc) g2
