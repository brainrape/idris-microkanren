module PipeOperators

%access public export

infixr 9 <||
infixl 9 ||>

infixr 0 <|
infixl 0 |>

(<||) : (b -> c) -> (a -> b) -> (a -> c)
(<||) f g x = f (g x)

(||>) : (a -> b) -> (b -> c) -> (a -> c)
(||>) f g x = g (f x)


(<|) : (a -> b) -> a -> b
(<|) f x = f x

(|>) : a -> (a -> b) -> b
(|>) x f = f x
