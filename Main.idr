module Main

import PipeOperators
import MicroKanren



wrap : (() -> Goal a) -> Goal a
wrap f =
    \st =>
            Immature (\() => (f ()) st)


fives : Term Int -> Goal Int
fives =
    \x =>
        (disjoin
            (identical x (LVal 5))
            (wrap (\() => fives x))
        )


step : MicroKanren.Stream a -> (Maybe (State a), MicroKanren.Stream a)
step Empty = (Nothing, Empty)
step (Immature f) = step (f ())
step (Mature st s) = (Just st, s)


headS : MicroKanren.Stream a -> Maybe (State a)
headS =
    step ||> fst


tailS : MicroKanren.Stream a -> MicroKanren.Stream a
tailS =
    step ||> snd


takeS : Nat -> MicroKanren.Stream a -> List (Maybe (State a))
takeS n s =
    case n of
        S n' =>
            headS s :: takeS (n') (tailS s)
        Z =>
            reverse <| headS s :: []


main : IO ()
main = do
    printLn (takeS 3 ((callFresh fives) ([], 0)))
    printLn (unify (LVar 0) (LVal "banana") [])
